#ifndef WRITEN_H
#define WRITEN_H
#include <sys/types.h>

ssize_t writen(int fd, const void *ptr, size_t n);
#endif // WRITEN_H
