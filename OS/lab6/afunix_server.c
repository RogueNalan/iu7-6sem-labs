#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

#define SOCK_NAME "socket.soc"
#define BUF_SIZE 256

int main(int argc, char ** argv)
{
  struct sockaddr server_name, client_name;
  char buf[BUF_SIZE];
  int   sock;
  int   namelen, bytes;

  sock = socket(AF_UNIX, SOCK_DGRAM, 0);
  if (sock < 0) 
  {
    perror("socket failed");
    return EXIT_FAILURE;
  }
  server_name.sa_family = AF_UNIX;
  strcpy(server_name.sa_data, SOCK_NAME);
  if (bind(sock, &server_name, strlen(server_name.sa_data) +
        sizeof(server_name.sa_family)) < 0) 
  {
    perror("bind failed");
    return EXIT_FAILURE;
  }
  namelen = sizeof(client_name);
  bytes = recvfrom(sock, buf, sizeof(buf),  0, &client_name, &namelen);
  if (bytes < 0) 
  {
    perror("recvfrom failed");
    return EXIT_FAILURE;
  }
  buf[bytes] = 0;
  client_name.sa_data[namelen] = 0;
  printf("Client sent: %s\n", buf);
  close(sock);
  unlink(SOCK_NAME);
}
 
