#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

#define SOCK_NAME "socket.soc"
#define BUF_SIZE 256

int main(int argc, char ** argv)
{
  int sock;
  sock = socket(AF_UNIX, SOCK_DGRAM, 0);
  char buf[BUF_SIZE];
  struct sockaddr server_name;

  if (sock < 0) 
  {
    perror("socket failed");
    return EXIT_FAILURE;
  }
  server_name.sa_family = AF_UNIX;
  strcpy(server_name.sa_data, SOCK_NAME);
  strcpy(buf, "Hello, world!");
  sendto(sock, buf, strlen(buf), 0, &server_name,
    strlen(server_name.sa_data) + sizeof(server_name.sa_family));
}
