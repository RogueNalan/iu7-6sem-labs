(defun sieve_of_eratosthenes (minimum maximum)
	(let ((sieve (make-array (+ 1 maximum) :element-type 'bit :initial-element 0)))
		(loop for i from 2 to minimum
			when (zerop (bit sieve i))
				do (loop for j from (expt i 2) to maximum by i
						do (setf (bit sieve j) 1)
					)
		)
		(loop for i from (1+ minimum) to maximum
			when (zerop (bit sieve i))
				collect i
				and do (loop for j from (expt i 2) to maximum by i
							do (setf (bit sieve j) 1)
						)
		)
	)
)

(defun easy-check-rec (x num max)
	(cond 	((>= num max) T)
			((= (mod x num) 0) Nil)
			(T (easy-check-rec x (+ num 2) max))))

(defun easy-check (x)
	(easy-check-rec x 3 (ceiling (sqrt x))))

	

(defun finder-rec (curr max check-func result)
	(cond 	((> curr max) result)
			((apply check-func (list curr)) (finder-rec (+ curr 2) max check-func (cons curr result)))
			(T (finder-rec (+ curr 2) max check-func result))))
			
(defun finder (min max check-func)
	(reverse (finder-rec (if (evenp min) (+ min 1) min) max check-func Nil)))