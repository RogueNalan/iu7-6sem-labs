(defun pow-expansion-rec (num count res)
	(cond 	((= num 0) res)
			((evenp num) (pow-expansion-rec (/ num 2) (+ count 1) res))
			(T (pow-expansion-rec (floor num 2) (+ count 1) (cons count res)))))

(defun pow-expansion (num)
	(pow-expansion-rec num 0 Nil))
	
(defun mod-pow-rec (a n mod res)
	(cond 	((= n 0) res)
			((evenp n) (mod-pow-rec (mod (* a a) mod) (/ n 2) mod res))
			(T (mod-pow-rec a (- n 1) mod (mod (* a res) mod)))))

(defun mod-pow (a n mod)
	(let 	((a-new (mod a mod))
			(n-new (mod n mod)))
			(mod-pow-rec a-new n mod 1)))

(defun mod-key (num key)
	(mod-pow num (car key) (cdr key)))
	
(defun get-k (art num)
	(cond	((= num 3) (cond ((= art 1) 2) ((= art 2) 1)))
			((= num 5) (cond ((= art 1) 4) ((= art 2) 2) ((= art 3) 3) ((= art 4) 1)))))
			
(defun get-inverse (num mod)
	(/ (+ (* mod (get-k (mod mod num) num)) 1) num))

(defun create-list-rec (x num result)
	(if (= x 0) result (create-list-rec (floor x num) num (cons (mod x num) result))))

(defun create-list (x chars-in-cell)
	(let 	((num (expt 10 (+ chars-in-cell chars-in-cell))))
			(create-list-rec x num Nil)))


(defun my-mult-rec (a b res)
	(cond 	((= b 0) res)
			((evenp b) (my-mult-rec (+ a a) (/ b 2) res))
			(T (my-mult-rec a (- b 1) (+ res a)))))

(defun my-mult (a b)
	(my-mult-rec a b 0))
	
(defun count-digits-rec (x res)
	(if (< x 10) res (count-digits-rec (floor x 10) (* res 10))))

(defun count-digits (x)
	(count-digits-rec x 1))

(defun find-min-mult (a b)
	(let(
			(am (count-digits a))
			(bm (count-digits b))
		)
		(if (< am bm) am bm)))
		
	
(defun karatsuba (a b c d mult)
	(let 	(	
				(ac (* a c))
				(bd (* b d))
			)
		(+ (* ac (* mult mult)) (* mult (- (* (+ a b) (+ c d)) ac bd)) bd)))

(defun karatsuba-mult (a b)
		(let 	((mult (find-min-mult a b)))
				(karatsuba (floor a mult) (mod a mult) (floor b mult) (mod b mult) mult)))

(defun encrypt (message public-key)
	(mod-key message public-key))

(defun decrypt (message private-key)
	(mod-key message private-key))
	
(defun encrypt-all (message-list public-key)
	(mapcar #'(lambda (x) (encrypt x public-key)) message-list))

(defun decrypt-all (message-list private-key)
	(mapcar #'(lambda (x) (decrypt x private-key)) message-list))