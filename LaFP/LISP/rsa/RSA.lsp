;Генерация ключей

;Вычислить модуль (произведение)
(defun production_numbers (num1 num2)
	(* num1 num2)
)

;Вычислить функцию Эйлера
(defun get_function_eiler (num1 num2)
	(* (- num1 1) (- num2 1))
)

;Выбрать открытую экспоненту
(defun open_exponent ()
	3)
	
	
;Получить обратный по умножению элемент в кольце вычетов по модулю	
(defun inverse (tt newt r newr)
	(if (/= 0 newr)
		(inverse newt (- tt (* (floor r newr) newt)) newr (- r (* (floor r newr) newr)))
		(cond
			((> r 1) 0)
			(T tt) 
		)
	)
)
	
	
;Вычислить секретную экспоненту
(defun get_secret_exponent (exponenta phi &aux(d (inverse 0 1 phi exponenta)))
	(cond 
		((> d 0) d)
		((< d 0) (+ d phi))
		(T 0)
	)
)

;Опубликовать открытый ключ. Вернёт точечную пару в виде e.n
(defun open_key (num1 num2)
	(cons (open_exponent)(production_numbers num1 num2))
)

;Сохранить закрытый ключ. Вернёт точечную пару в виде d.n
(defun close_key(num1 num2)
	(cons (get_secret_exponent (open_exponent)(get_function_eiler num1 num2))(production_numbers num1 num2))
)