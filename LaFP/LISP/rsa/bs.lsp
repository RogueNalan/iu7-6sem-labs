(defun random_element (list)
  (nth (random (length list)) list))

(defun readd (filename)
	(let ((in (open filename :if-does-not-exist nil)) (res 0))
	   (when in
			(loop for cr = (read-char in nil)
				while cr do
					(cond
						((char-equal cr #\space) (setf res (+ (* res 100) 10)))
						((char-equal cr #\() (setf res (+ (* res 100) 11)))
						((char-equal cr #\)) (setf res (+ (* res 100) 12)))
						((and(char<= cr #\z)(char>= cr #\A)) (setf res ( - (+ 13 (* res 100) (char-code cr)) (char-code #\A))))
						(T nil)
					)
			)
			(close in)
	   )
	   res
	)
)

(defun cou (ff cr)
	(cond
		((= cr 10) (write-char #\space ff))
		((= cr 11) (write-char #\( ff))
		((= cr 12) (write-char #\) ff))
		((and(<= cr 70)(>= cr 13)) (write-char (code-char (+ cr (char-code #\A) -13)) ff))
		(T nil)
	)
)


(defun co (ff data)
	(progn
		(when (>= data 100)
			(co ff (floor(/ data 100)))
		)
		(cou ff (mod data 100))
	)
)

(defun writed (filename data)
	(let ((out (open filename :direction :output :if-exists :supersede)))
		(co out data)
		(close out)
	)
)

