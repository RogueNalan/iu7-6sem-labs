(defun writefile(fname data)
	(let ((out (open fname :direction :output :if-exists :supersede)))
		(format out "~a" data)
		(close out)
	)
)

(defun readfile(fname)
	(with-open-file (in fname :if-does-not-exist nil)
		(parse-integer (read-line in "0"))
	)
)


(defun encryptfile (ifname ofname openkey)
	(writefile ofname (encrypt (readd ifname) openkey))
)

(defun decryptfile (ifname ofname closekey)
	(writed ofname (decrypt (readfile ifname) closekey))
)