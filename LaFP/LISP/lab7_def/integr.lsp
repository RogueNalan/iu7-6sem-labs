(defun get-rectangle-value (a b func)
	(float (* (apply func (list (/ (+ a b) 2))) (- b a))))
	
(defun mid-rectangle-rec (a b step func min max res)
	(cond	((> b max) (if (< (abs (- a max)) 0.001) res (* (get-rectangle-value a max func) res)))
			(T (mid-rectangle-rec (+ a step) (+ b step) step func min max (+ (get-rectangle-value a b func) res)))))
			
(defun mid-rectangle (min max step func)
	(if (or (>= min max) (= step 0)) `Error!
		(mid-rectangle-rec min (+ min step) step func min max 0)))
		
(defun get-trap-value (a b func)
	(float (* (/ (+ (apply func (list a)) (apply func (list b))) 2) (- b a))))
	
(defun trap-rec (a b step func min max res)
	(cond	((> b max) (if (< (abs (- a max)) 0.001) res (* (get-trap-value a max func) res)))
			(T (trap-rec (+ a step) (+ b step) step func min max (+ (get-trap-value a b func) res)))))
			
(defun trap (min max step func)
	(if (or (>= min max) (= step 0)) `Error!
		(trap-rec min (+ min step) step func min max 0)))
		
(defun create-list-rec (max curr step res)
	(if (or (> curr max) (< (abs (- curr max)) 0.001)) (reverse res)
			(create-list-rec max (+ curr step) step (cons curr res)))
)


(defun mid-rectangle-func (min max step func)
	(let	(
				(mylist (create-list-rec max (+ min (/ step 2)) step Nil))
			)
	(reduce #'+ (mapcar #'(lambda (x) (* (apply func (list x)) step)) mylist)))
)

(defun trap-func (min max step func)
	(let
		(
			(mylist (append (create-list-rec max min step Nil) (list max)))
		)
	(reduce #'+
		(mapcon #'(lambda (x)
						(if (null (cdr x)) Nil
										(let
										(
											(currv (apply func (list (car x))))
											(nextv (apply func (list (cadr x))))
											(diff (- (cadr x) (car x)))
										)
										(list (* (/ (+ currv nextv) 2) diff))))) mylist))))