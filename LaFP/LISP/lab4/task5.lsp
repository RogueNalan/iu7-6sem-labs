(defun simple-roll () (list (+ (random 6) 1) (+ (random 6) 1)))
(defun rerollp (x) (if (or (equal x '(1 1)) (equal x '(6 6))) (simple-roll) x))
(defun roll() (rerollp (simple-roll)))

(defun sum (x) (cons (+ (car x) (cadr x)) x))
(defun winp (x) (or (= (car x) 7) (= (car x) 11)))

(defun sum-roll () (sum (roll)))

(defun winnerp (first second)
	(cond 	((winp (print first)) '(First player won))
			((winp (print second)) '(Second player won))
			((> (car first) (car second)) '(First player won))
			((= (car first) (car second)) '(There is a tie))
			(T '(Second player won))))

(defun game () (winnerp (sum-roll) (sum-roll)))