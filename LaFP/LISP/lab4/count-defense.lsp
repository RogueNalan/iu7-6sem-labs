(defun find-min-time (curr min-time)
				     (cond  ((eq curr Nil) min-time)
							((< (cadar curr) min-time) (find-min-time (cdr curr) (cadar curr)))
							(T (find-min-time (cdr curr) min-time))))
							
(defun find-min-list-time (names-list)
				(find-min-time (cdr names-list) (cadar names-list)))
				
(defun calc-time 	(curr sum)
					(if (eq curr Nil) sum (calc-time (cdr curr) (+ sum (cadar curr)))))

(defun calc-list-time (names-list) (calc-time names-list 0))					

(defun get-all-time (names-list)
					(cond 	((eq names-list Nil) Nil)
							((= (length names-list) 1) (cadar names-list))
							(T (+ (calc-list-time names-list) (* (- (length names-list) 3) (find-min-list-time names-list))))))