(defun find-min-name (curr min-time min-name)
				     (cond  ((eq curr Nil) min-name)
							((< (cadar curr) min-time) (find-min-name (cdr curr) (cadar curr) (caar curr)))
							(T (find-min-name (cdr curr) min-time min-name))))

(defun find-min (names-list)
				(cond 	((eq names-list Nil) Nil)
						((eq (cdr names-list) Nil) (caar names-list))
						(T (find-min-name (cdr names-list) (cadar names-list) (caar names-list)))))
						
(defun get-appender (min-name) 
	(lambda (curr) (if (not (eq min-name (car curr))) (list (list (car curr) min-name) min-name) Nil)))
						
(defun get-explicit-list (names-list) 
	(mapcar (get-appender (find-min names-list)) names-list))

(defun get-removed-list (names-list) 
	(remove-if #'(lambda (x) (eq (car x) Nil)) (get-explicit-list names-list)))

(defun remove-last (x) 
	(reverse (cdr (reverse x))))

(defun append-if-necessary (result names-list min-name) 
	(if (eq (caar (last names-list)) min-name) 
	(append (butlast result) (butlast (car (last result))))
	(append (butlast result) (list (list (caar (last names-list)) min-name)))))

(defun get-list (names-list) (append-if-necessary (get-removed-list names-list) names-list (find-min names-list)))

(setf test `((Ермаков 10) (АТеплов 10) (Селедцов 20) (Баранов 2)))