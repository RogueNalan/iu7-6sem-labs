/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB3
 FileName: LAB3.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab3.inc"

domains
	ownership = house(symbol, real Square);
		    car(symbol, real Horses);
		    computer (symbol, real Performance).

predicates

  owner (symbol, integer, ownership, real).

  tax (ownership, real).
  
  all_householders(symbol).
  
  next_node (symbol, integer, integer, real).
  max_node_ind (symbol, integer).
  sum_all_impl(symbol, integer, real).  
  sum_all(symbol, real).
    
clauses
  
  owner (abc, 0, house (moscow, 50.5), 10000.1).
  owner (abc, 1, car (volvo, 120.5), 3000.95).
  owner (abc, 2, computer (laptop, 66.5), 700.0).
  owner (cde, 3, house (petersburg, 71.4), 15000.0).
  owner (cde, 4, computer (pc, 54.9), 423.2).
  owner (efg, 5, car (toyota, 98.2), 2000.0).
  owner (abc, 6, house (moscow, 60.0), 12500.7).
  
  tax (house (_, _), 0.15).
  tax (car (_, _), 0.1).
  tax (computer (_, _), 0.01).
  
  all_householders (Name) :- owner (Name, _, house (_, _), _).
  
  next_node (Name, StartIndex, NextIndex, TaxCost) :- owner (Name, NextIndex, Ownership, Cost),
  						      NextIndex > StartIndex,
  						      tax (Ownership, TaxValue),
  						      TaxCost = Cost * TaxValue,
  						      !.
  
  max_node_ind (abc, 6).
  max_node_ind (cde, 4).
  max_node_ind (efg, 5).
  
  sum_all_impl (Name, CurrIndex, Res) :- max_node_ind (Name, MaxIndex), MaxIndex = CurrIndex, Res = 0.
  sum_all_impl (Name, CurrIndex, Res) :- next_node (Name, CurrIndex, NextIndex, TaxCost),
  					 sum_all_impl (Name, NextIndex, TaxCost1),
  					 Res = TaxCost + TaxCost1.
  
  sum_all (Name, Result) :- sum_all_impl (Name, -1, Result).
  
goal
  sum_all(abc, X).