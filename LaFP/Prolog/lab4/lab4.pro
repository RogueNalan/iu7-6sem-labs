/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB4
 FileName: LAB4.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab4.inc"

predicates
  
  factorial_impl(integer, integer, integer, integer).
  factorial (integer, integer).

  max (real, real, real).
  max (real, real, real, real).
  
  fibonacci_impl (integer, integer, integer, integer).
  fibonacci (integer).
  
clauses
  
  factorial_impl(X, Curr, MaxX, Curr) :- X > MaxX, !.
  factorial_impl(X, Curr, MaxX, Res) :- Curr1 = Curr * X, X1 = X + 1, factorial_impl(X1, Curr1, MaxX, Res).
  
  factorial(X, Res) :- factorial_impl(2, 1, X, Res).
    
  max (A, B, Ans) :- A > B, Ans = A, !.
  max (A, B, Ans) :- B > A, Ans = B, !.
  max (A, A, Ans) :- Ans = A.
  
  max (A, B, C, Ans) :- A >= B, A >= C, Ans = A, !.
  max (A, B, C, Ans) :- B >= A, B >= C, Ans = B, !.
  max (A, B, C, Ans) :- C >= A, C >= B, Ans = C, !.
  max (A, A, A, Ans) :- Ans = A.

  fibonacci_impl(_, _, N, MaxN) :- N > MaxN, !.

  fibonacci_impl(X1, X2, N, MaxN) :- 
    write(X1),
    Curr = X1+X2,
    N1 = N+1,
    fibonacci_impl(X2, Curr, N1, MaxN).
  
  fibonacci(MaxN) :- fibonacci_impl(0, 1, 0, MaxN).
  
goal
  factorial(5, Res).