zerodiscr(D) :- D > -0.001, D < 0.001.

solvediscr(A, B, D, X) :- zerodiscr(D), X is -B/2/A.
solvediscr(A, B, D, X) :- D < 0.001, writeln('No solutions!'), !.
solvediscr(A, B, D, X) :- not(zerodiscr(D)), X is (D - B)/2/A.
solvediscr(A, B, D, X) :- not(zerodiscr(D)), X is (-D - B)/2/A.

solve(0, 0, 0, _) :- writeln('Any number!').
solve(0, 0, _, _) :- writeln('No solutions!').
solve(0, B, C, X) :- X is -C/B.
solve(A, B, C, X) :- solvediscr(A, B, B*B - 4*A*C, X).