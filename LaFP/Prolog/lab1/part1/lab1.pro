/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB1
 FileName: LAB1.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab1.inc"

predicates

  student (symbol, symbol, symbol, symbol, symbol, real, symbol).
  
  group (symbol, symbol).
  district (symbol, symbol).
  
  grantmean (symbol).
  grantinfo (symbol).
  grantL(symbol).
  
  compare (symbol, symbol, symbol, symbol, real, symbol,
           symbol, symbol, symbol, symbol, real, symbol).
  same(symbol, symbol, symbol).
    
clauses

  student (balashova, sophia, antonovich, iu763, zelenograd, 4.5, no).
  student (baryshnikov, andrew, igorevich, iu763, izmailovo, 4.75, no).
  student (vodyaha, andrew, antonovich, iu761, izmailovo, 3.75, yes).
  student (katrin, michael, antonovich, iu763, otradnoye, 4.0, no).
  student (stolyarenko, artem, vladimirovich, iu761, otradnoye, 3.25, no).
  student (teplov, alexander, petrovich, iu763, otradnoye, 4.0, yes).
  
  group (WhoN, Name) :- student (WhoN, Name, _, _, _, _, _).
  
  district (WhoD, District) :- student (WhoD, _, _, _, District, _, _).
  
  grantmean (WhoM) :- student (WhoM, _, _, _, _, Mean, _), Mean >= 4.
  grantinfo (WhoI)  :- student (WhoI, _, _, _, _, _, yes).
  grantL(X) :- student(X,_,_,_,_,Mean,_),Mean>=4.
  grantL(X) :- student(X,_,_,_,_,_,Info),Info=yes.
  
  compare(SName1, Who1, Gr1, Dis1, Mean1, Info1,
          SName2, Who2, Gr2, Dis2, Mean2, Info2) :- 
                  SName1 = Sname2,
                  Who1 = Who2,
                  Dis1 = Dis2,
                  Mean1 = Mean2,
                  Info1 = Info2.
    
  same(Who, Name1, Otch) :-
               student(Who, Name1, Otch, Gr1, Distr1, Mean1, Info1),
               student (Who2,Name2, Otch, Gr2, Distr2, Mean2, Info2),
               NOT (compare (Who, Name1, Gr1, Distr1, Mean1, Info1,
                            Who2, Name2, Gr2, Distr2, Mean2, Info2)).
goal

%    group (WhoN, iu761).
%   district (WhoD, izmailovo).
%  grantmean(WhoM), grantinfo (WhoI).
%  same(SName, NameP, OtchP).
   same (Who, Name, Otch).