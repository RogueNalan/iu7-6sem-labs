/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB2
 FileName: LAB2.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab2.inc"

predicates
  deposit (symbol, symbol, integer).
  depositor(symbol, symbol, integer).
  
clauses

  deposit (artem, sberbank, 10000).
  deposit (andrew, vtb, 100500).
  deposit (igor, moscowbank, 347654).
  deposit (alexander, sberbank, 3000).
  deposit (artem, vtb, 5000).

  depositor(Who, Bank, Sum) :- deposit (Who, Bank, DepMoney), DepMoney > Sum.
  
goal

  depositor(Who, Bank, 4000).
