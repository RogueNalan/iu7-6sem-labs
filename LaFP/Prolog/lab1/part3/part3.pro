/*****************************************************************************

		Copyright (c) My Company

 Project:  PART3
 FileName: PART3.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "part3.inc"

predicates
  car (symbol, integer, real).

clauses
  car (bmwx5, 22000, 100.0).
  car (nissanjuke, 12000, 80).
  car (mercedesbenz, 25000, 121.4).
  car (var2109, 1500, 72.6).
  car (lexusis, 21000, 208.5).

goal
  car (Name, Cost, Horses), Cost < 20000, Horses > 75.