/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB2
 FileName: LAB2.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab2.inc"

predicates

  person (symbol, symbol).
  parent (symbol, symbol).
  
  grandfathers(symbol, symbol).
  grandmothers(symbol, symbol). 
  grands(symbol, symbol).

clauses
  person("Judith",female).
  person("Bill",male).
  person("John",male).
  person("Pam",female).
  parent("John","Judith").
  parent("Bill","John").
  parent("Pam","Bill").

  grands (Name, GrandName) :- parent (GrandName, X), parent (X, Name).
  
  grandfathers (Name, GrandName) :- grands (Name, GrandName), person (GrandName, Gender), Gender = male.
  grandmothers (Name, GrandName) :- grands (Name, GrandName), person (GrandName, Gender), Gender = female.

goal

  grandmothers ("Judith", GrandName).
