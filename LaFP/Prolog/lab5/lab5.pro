/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB5
 FileName: LAB5.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab5.inc"

domains
  numlist = integer*.
 
  operation = leave_less; leave_more.
 
predicates

  max(numlist, integer).
  max_impl(numlist, integer, integer).
  
  sum(numlist, integer).
  sum_impl(numlist, integer, integer).
  
  reverse(numlist, numlist).
  reverse_impl(numlist, numlist, numlist).
  
  filter (numlist, integer, operation, numlist).
  
  filter_less (numlist, integer, numlist, numlist).
  filter_more (numlist, integer, numlist, numlist).
  
  nth(numlist, integer, integer).
  nth_impl(numlist, integer, integer, integer).

  append(numlist, numlist, numlist).

  delete(numlist, integer, numlist).
  
  bubblesort(numlist, numlist).
  bubblesort_impl(numlist, numlist, numlist).
  bsort_iter(integer, numlist, numlist, integer).
  
  qsort(numlist, numlist).
  split(integer, numlist, numlist, numlist).
    
clauses

  max([Head|Tail], Result) :- max_impl (Tail, Head, Result).

  max_impl([], Result, Result) :- !.
  max_impl([Head|Tail], Max, Result) :- Head > Max, max_impl(Tail, Head, Result), !.
  max_impl([Head|Tail], Max, Result) :- max_impl (Tail, Max, Result).

  sum(List, Sum) :- sum_impl(List, 0, Sum).

  sum_impl([], Result, Result) :- !.
  sum_impl([Head|Tail], Temp, Result) :- NewTemp = Temp + Head, sum_impl(Tail, NewTemp, Result).
  
  reverse(List, Result) :-
  	reverse_impl(List, [], Result). 
 
  reverse_impl([], Result, Result):- !.
  reverse_impl([Head|Tail], Buffer, Result) :- reverse_impl (Tail, [Head|Buffer], Result).

  filter(List, FiltNum, leave_less, Result) :- filter_less(List, FiltNum, [], Result).
  filter(List, FiltNum, leave_more, Result) :- filter_more(List, FiltNum, [], Result).
  
  filter_less([], _, Result, Result).
  filter_less([Head|Tail], FiltNum, Buffer, Result) :- Head < FiltNum, filter_less(Tail, FiltNum, [Head|Buffer], Result), !.
  filter_less([Head|Tail], FiltNum, Buffer, Result) :- filter_less(Tail, FiltNum, Buffer, Result).

  filter_more([], _, Result, Result).
  filter_more([Head|Tail], FiltNum, Buffer, Result) :- Head > FiltNum, filter_more(Tail, FiltNum, [Head|Buffer], Result), !.
  filter_more([Head|Tail], FiltNum, Buffer, Result) :- filter_more(Tail, FiltNum, Buffer, Result).

  nth (List, Max, Result) :- nth_impl(List, -1, Max, Result).
  
  nth_impl([Head|Tail], Max, Max, Head) :- !.
  nth_impl([Head|Tail], Curr, Max, Result) :- NewCurr = Curr + 1, nth_impl(Tail, NewCurr, Max, Result).
 
  append([], Result, Result) :- !.
  append([Head|Tail], AppList, [Head|Result]) :- append(Tail, AppList, Result).
  
  delete([], Number, []) :- !.
  delete([Number|Tail], Number, Result) :- delete(Tail, Number, Result), !.
  delete([Head|Tail], Number, [Head|Result]) :- delete(Tail, Number, Result).
  
  bubblesort(List, Result) :- bubblesort_impl(List, [], Result).
  
  bubblesort_impl([], Buffer, Buffer).
  bubblesort_impl([Head|Tail], Buffer, Result) :-
  	bsort_iter(Head, Tail, NewTail, Max),
  	bubblesort_impl(NewTail, [Max|Buffer], Result).
  
  bsort_iter(X, [], [], X).
  bsort_iter(X, [Y|Tail], [Y|NewTail], Max) :- X > Y, bsort_iter(X, Tail, NewTail, Max), !.
  bsort_iter(X, [Y|Tail], [X|NewTail], Max) :- bsort_iter(Y, Tail, NewTail, Max).
  
  qsort([], []).
  qsort([Head|[]], [Head]) :- !.
  qsort([Head|Tail], Result) :- split(Head, Tail, LesserList, OthersList),
  				append(OthersList, [Head], FullOthers),
  				qsort(LesserList, TempLesser),
  				qsort(FullOthers, TempOthers),
  				append(TempLesser, TempOthers, Result).
  				
  split(_, [], [], []).
  split(Value, [Head|Tail], [Head|Lesser], Others) :- Value > Head, !, split (Value, Tail, Lesser, Others).
  split(Value, [Head|Tail], Lesser, [Head|Others]) :- split(Value, Tail, Lesser, Others).
  
  goal
  
  qsort([7,12,8,1,3,9,0,-4], Result).