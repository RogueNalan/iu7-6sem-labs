sourceT = load('dataT.txt');
sourceY = load('dataY.txt');
apprLevel = 1:3;

coefs = zeros(apprLevel(end), size(sourceT, 2));

for i=apprLevel
   coefs(i, :) = sourceT .^ (i - 1);
end;

% for j=1:length(sourceT)
%     if (sourceT(j) < 2 && sourceT(j) > -2)
%         sourceY(j) = 200;
%     end;
% end;

theta = transpose(coefs) \ transpose(sourceY);

apprY = zeros(1, size(sourceT, 2));
for i=apprLevel
    apprY = apprY + theta(i) * (sourceT .^ (i - 1));
    fprintf('power = %i, coef = %f\n', i, theta(i));
end;
deviation = sqrt(sum((sourceY - apprY) .^ 2));
fprintf('deviation = %f\n', deviation);

subplot(1, 1, 1);
hold on;
plot(sourceT, sourceY, '.');
plot(sourceT, apprY);
hold off;
grid on;