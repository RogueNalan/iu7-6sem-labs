X = load('data123.txt');
Mmax = max(X);
Mmin = min(X);
R = Mmax-Mmin;
mu = mean(X);
S2 = var(X);
m = floor(log(length(X))/log(2))+2;
subplot(2,1,1);
histfit(X, m);

[ef,ex] = ecdf(X);
Xx=Mmin:0.1:Mmax;
F1= normcdf(Xx,mu,S2);
subplot(2,1,2);
hold on
plot(Xx,F1)
stairs(ex, ef)
hold off
grid on

