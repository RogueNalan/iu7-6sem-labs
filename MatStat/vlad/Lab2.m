X = load('data123.txt');

GAMMA = 0.99;
ALPHA = (1 - GAMMA)/2;
Start = 10;
N = Start:length(X);
MeanX = mean(X);
VarX = var(X);
M = zeros(1, length(X)-Start+1);
for i=N
    M(i-Start+1) = mean(X(1:i));
end;
S = zeros(1, length(X)-Start);
for i=N
    S(i-Start+1) = var(X(1:i));
end;

subplot(1,2,1), plot([N(1), N(end)], [MeanX, MeanX], 'm');
hold on;
subplot(1,2,1), plot(N, M, 'g');
Ml = M - sqrt(S ./ N) .* tinv(1 - ALPHA, N - 1);
subplot(1,2,1), plot(N, Ml, 'b');
Mh = M + sqrt(S ./ N) .* tinv(1 - ALPHA, N - 1);
subplot(1,2,1), plot(N, Mh, 'r');
grid on;
hold off;
title('Mean');

subplot(1,2,2), plot([N(1), N(end)], [VarX, VarX], 'm');
hold on;
subplot(1,2,2), plot(N, S, 'g');
Sl = S .* (N - 1) ./ chi2inv(1 - ALPHA, N - 1);
subplot(1,2,2), plot(N, Sl, 'b');
Sh = S .* (N - 1) ./ chi2inv(ALPHA, N - 1);
subplot(1,2,2), plot(N, Sh, 'r');
grid on;
hold off;
title('Variance');
num2str(Sl(length(Sl)))
num2str(Sh(length(Sh)))
